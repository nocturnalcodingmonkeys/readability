# READ ME #

## Readability module ##

The Readability (modules) has the methods to determine the readability of a given string (of text).  All the methods are extensions to the `string` class (old Xojo framework) and to the `text` class (new Xojo framework).  It also includes all the methods to do all the counts that the various Scores/Indexes need to do.  That way those counts can be used in other programs.

we split the methods into two modules, one for `string` and one for `text`.  just include the one that you need (old vs new frameworks).  you can add both as they dont conflict with each other.

### current Readability Scores/Indexes: ###
The entries are in the format of `Readability Score (old framework / new framework)` where the version number is the version the method was added to the old or new framework.
* Coleman Liau Index (**v20161204** / **20180219**)
* Flesh Kincaid (**v20161204** / **20180219**)
* Gunning Fog (**v20161204** / **20180219**)
* U.S. Army FORECAST (**v20161204** / **20180219**)

### testing ###
currently there is no testing of the scores to make sure the are accurate.  In one of the future versions we will add XojoUnit tests against known texts/scores to make sure we are calculating the scores correctly.

### optimization ###
currently the methods are not optimized.  All methods are using the first try to complete the method over using the best method.  Optimizations will come in the future.

### source of formulas ###
* [Coleman Liau Index](https://en.wikipedia.org/wiki/Coleman%E2%80%93Liau_index)
* [Flesh Kincaid](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests)
* [Gunning Fog](https://en.wikipedia.org/wiki/Gunning_fog_index)
* [U.S. Army FORECAST](https://en.wikipedia.org/wiki/Readability)
